-----BEGIN PGP PUBLIC KEY BLOCK-----

mQGNBF5W5xABDAC5p6EcCX/tPRv1xcXFEvDxIy+kW8aZ3qvUQxmaVwG+GyttTG4V
zLLY6mFVPTbWNpFFyNv2NF0weOj+XklTh1OukoRHOdr13gfz/TBFR2O0F2x/Hwx9
TVUX2SQMSBR/puoHncMiSGvidD3Kk2i88zhOcfI0im9kUDdq5jko9JoqGiCqdpp5
BV3Ti1ufGMJDIjkX2/fXSkJVzmuWoV6596XFKtBlt2CSMIpwXcUgJCUwLNl8yML3
xwfJKFiRB0PEPkGb+HrzUwGpvtQcHcxMM5kkWidzE+PyGhh+NPZwd4laiFzCF14H
PEtgbaem1KrrPHdSleT+CAY4r/IB9vFpWP3rp8aUYXJdHjeWgAS/AVr4PCKSjFMt
JbWZsll5FL2mYLlFWzL3PR6LRV26s2tOEgPykDJchyoLsc2Mng+TDqBnrggBGPYR
ksTikcM2SAJ2ctliBkVbROYSf9BTqm8BPTaIk9mfCsU6BX8vzaBdd0wnARPm7Qp8
Q5N3JV6MyB5JQdUAEQEAAbQlQnJpYW4gRGlja2VucyA8YnJpYW5AaG9zdGlsZWZv
cmsuY29tPokB1AQTAQoAPhYhBFmeSJJF0psihUwOfx0dscBR+ynMBQJeVucQAhsD
BQkDwmcABQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJEB0dscBR+ynMe5cL+wdd
jD7tWlLEC7JsWlHR9WmzE3tbcos4y/Fay7ThrdtwNHISThf57qtgrlKdblEhs5GG
FVoe9un/xkmrWsR4i1j1P5dQ5fJVWRINl4e54F7vw0B/lNS+o1nlBn9ZzZkscCZw
VCVxLWvKyyImzBn++5i50dW8aVt74IiaRJ+RX+YG83/+UzEKeBjZNJLfHpye+r0x
klA/MLfyB1O/w8vhTimsoEW/sRvu2XaTAfsKgQ2bUvrtwt2D+bQIqubTNaia2iQJ
/wilqSmoMQHJWelX35XQHvzpsZAFK+p2X1Wzx+NxTWrg+yBhl1Swp1EVvJTA8QXe
mdFjYH0Zqq04S7mWMl2W7cRXFaK+1m7/zRoQxUgt5aeIt/0c7o5tpA/1ZpJZXGaW
A/KUuG90jeQ2HlUZ3JYZTI7WPDX684zN9Zcnrcem7O7Jzn5oQx7rce96ezddY+7Z
xzf4ofuaWszr/AHFq0nKBxclP2xhhEfWBc/cINers91SgS10BsaBnvnXyZYbDrkB
jQReVucQAQwAxEYyOzGMFN2ezf/6Qzs8UNOdiRJYw9UIVBP6UCOtX5E5fdqDbpPH
VFmIjZRX84aVMYMgLJnhehaDfzQHYGZ4v2B8tnrbh0i/U5FOzM3iKglE3Kkn6gzw
pFmi9CUyasV+7EfBopvJv7reWiBHZrhTUL+a8K8zRE+IAUl4Yib6WA5UYZIZ/kFM
ZOuLSNBPwwWFca2+bXamTMP68njpdFv/I7XCCJgH266Yx5qKrYBDaRYGlg84pm6f
U+p2Iw1k5BK3yXZ9sZ98LESY2b9HTI9KUtwwtXCoop7vkW/PR48BL6nhdZ0lU9u9
VlE946vE7Xk2EffwUr/uyYLwoDLLeDtJEmaJLEuMaU5Ws+GOUYBtflqx1LxGnXDb
ulr9l78WZOlyUhupt8e0/pGhqyMvbNTlXQhYsy2UPuQsXCrWxlWZdUaiyWhumhAq
33a4k+PreZF4mZYYjSt1BAL8moqVvQNhWrrijSMDm5V4xV2Z+Fxg4KNZJyuoAUvm
2bN02lRf+43xABEBAAGJAbwEGAEKACYWIQRZnkiSRdKbIoVMDn8dHbHAUfspzAUC
XlbnEAIbDAUJA8JnAAAKCRAdHbHAUfspzIlGC/9TGhxW8ENfTbuptNQcOLUBpksH
yLCS243WtgUt+UA+qwPnlAMh9lufER+4ePoOJhlA+s9Xp0PVudB9BuNtR3tS9U6u
40ScXwiR6y0h6DJDbfaZuepf9/6j9ZqOsXVcmrdKr522ZiaNRn1f0YYeL8iWfzbx
iRyVHM7SJZ6VHlrsncTakScmWUphgosQEdR/Yo6rfKXwGemBJKQxVBehPrPFauCH
O8culifvcJG6CUDyv/URs5RwPEo37scE6zEXwsXXPObxsBLMdKWbGTkayFReXv+J
vyrFNGfqd7ugX/uYL7jS97J8pSHuUIMFdfvbdXfWjJGjety1alj/B79WOI6DXrT8
8gIrWn3UUN5Jlm+JCd1Tt5S0z4NU45tV5G9viv+WVdle8y6LHOWd2MFMV2fJ4I3L
CIoEPHoT/iVCCuTJSu5uavEzI//VUqtiPEK+AKm+sD7pyhiDxAXAlUIi1ByDECcT
InEsvFSxGQR4kF7CCFn4lsepgflmac4ulpPWVPo=
=Suw/
-----END PGP PUBLIC KEY BLOCK-----

-----BEGIN PGP MESSAGE-----

hQGMA+cGE45lRfRiAQv+NWH+RNQerCHuJtZ6icyccmdgw4RNjx7S3HYHdyVGHy1u
j+0lVr5k3pYCb2/KdffdZk9vMyUCkjlit3YYjsxAkwhbOAxye8q5vB2zc4c9HHlN
oC1rHPN4/mCkmOWEzQe6n1qe3g/sVTOtNPVQpn5rHVsn08BGfJFGLamS9cCQNDb+
fCZTMtcx/5fau2eh6ISFx07mZcy39o5lFhmG0vWmD5c99gBkIbT5ppPedZURi8AE
zYnKGRrD3mdLCF4ZVt1vnuhu2p81QslhjKlJNiu7ebtV8K9K2GWbyOyd3geuxWz1
cKxYsrfqBnsgvaYsw7aYUKP9XNoBsw/1KB3uduhd05Cti6jXO4W2dl3x+CrRTfGG
i3v0f6gjIK04e0ufgWKSfEvgqFUid1lXWw62aOdC/A68rND/FI8HHBNJTWL+ZfEI
seVK6iBjsluQZPfk6MGjsq6/tBSbUEXxADtg51+XmMXeHrPh9GOZSYQyZARUa9VN
AJyBXkXm36jZ7bbq2tFN0pQBRgT/HxlbwUTSPASVS0kiXhCSNEo36LIAtNI9BvwQ
qk/iTm8H67SYiU3idatVODJVM73xtLqlDUx+6e5hHmzfkXfO/WSFGgswNU96evOS
e0KIwEd4CFWajckgg/y/uBp7AF/USyVBS0c3k/YqMIgp3BdKaJlGWY+GhV9gLHTK
z2EmTbf+Zy4T5p5S7sJlz8oMzGjA
=DGwK
-----END PGP MESSAGE-----
