Rebol [
    title: "Read messages in current room"
    file: %read-chat.reb
    date: 1-Mar-2020
    notes: {function to read messages from current room}
]

read-chat: func [chat-account [object!] roomno [integer!]
    <local> website obj response
][
    chat-account/room: roomno
    if not something? chat-account/sessionkey [ return "Not logged in"]

    if get in chat-account 'site [
        website: chat-account/site
        response: write to url! unspaced [website "/read"] reduce ['POST (mold chat-account)]
        return response: to text! response
        probe type-of response
        probe length-of response
        for-each msg response [
            print spaced ["Msg:" msg/messageid "User:" msg/userid "on" msg/date - now/zone]
            print head remove remove back back tail remove/part msg/content 2
        ]
    ] else [
        return "No chat-server specified yet.  Maybe you're not logged in?"
    ]
]
