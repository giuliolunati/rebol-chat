Rebol [
    title: "Library functions"
    file: %library.reb
    notes: {to hold various functions used by server}
    date: 24-Feb-2020
]

random/seed now/precise


read-chat: func [ request [binary!]
    {returns all the messages (currently) in specified room}
    <local> user sessionkey u msg roomno room data messages userlist lr
][
    -- "Inside read-chat"
    request: to text! request
    user: _ data: copy ""
    userlist: copy []
    for-each user users [
        repend userlist [user/userid user/username]
    ]
    probe userlist
    if did parse request [thru "sessionkey:" any space thru {"} copy sessionkey to {"} to end][
        for-each u users [
            if u/sessionkey = sessionkey [
                user: u
                break
            ]
        ]
        ; got a registered user request
        if something? user [
            ; now read the messages - rebol needs a trailing "/" for a directory read
            if did parse request [thru "room:" any space copy roomno numbers to end][
                -- roomno
                if exists? room: dirize to file! unspaced [what-dir "rooms/" roomno][
                    -- room
                    if err: trap [
                        ; get the last read message for that room
                        lr: make object! user/lastread ; easier to handle instead of a block
                        lrno: get in lr to word! join "room-" roomno
                        messages: sort lib/read room
                        -- "pre-read message"
                        probe user
                        for-each message-file messages [
                            if not dir? message-file [
                                 message: do load to file! unspaced [room "/" message-file]
                                 if message/messageid > lrno [
                                     append data spaced [
                                        "User:" select userlist message/userid "msgid:" message/messageid "date:" simple-date message/date newline
                                        remove-apostrophes message/content newline newline
                                    ]
                                ]
                                set in lr to word! join "room-" roomno message/messageid
                            ]
                        ]
                        ; set it back to a block
                        user/lastread: body-of lr
                        ; msg: mold data
                        -- "post read messages"
                        probe user
                        if empty? data [data: copy "No more messages"]
                        msg: data
                    ][
                        msg: mold err
                    ]
                ] else [
                    msg: spaced ["Room" roomno "does not exist"]
                ]
            ] else [
                msg: "Room no was not specified in a valid format."            
            ]
        ] else [
            msg: "you need to be logged in to do this"
        ]
    ] else [
        msg: "Invalid format - you need to provide a sessionkey"
    ]
    msg
]

whois-registered: func [ session-key
    {shows who is in the gpg keyring to those who are logged in.}
    <local> tempfile temp msg users username
][
    users: copy ""
    tempfile: form checksum/method to binary! session-key 'md5
    remove/part tempfile 1
    remove-apostrophes tempfile
    tempfile: to file! tempfile
    call/output "gpg --list-keys" tempfile
    if exists? tempfile [
        temp: to text! read tempfile
        parse temp [
            some [
                thru "uid" thru "[" thru "unknown" thru "]" any space copy username to "<" 
                (append users username append users newline)
            ]
        ]
        msg: copy users
    ] else [
        msg: "Unable to read list of users"
    ]
    attempt [delete tempfile]
    msg
]

fetch-youtube-title: func [ youtube-id youtube-key [text!]
    <local> data api-endpoint err
][
    if err: trap [
        api-endpoint: https://www.googleapis.com/youtube/v3/videos?part=snippet&id={YOUTUBE_VIDEO_ID}&fields=items%28id%2Csnippet%29&key={YOUR_API_KEY}
        replace api-endpoint "{YOUTUBE_VIDEO_ID}" youtube-id
        replace api-endpoint "{YOUR_API_KEY}" youtube-key
        data: to text! read to url! api-endpoint
        msg: if did parse data [thru "title" {":} any space {"} copy title to {"} to end][
            title
        ] else [
            "Rebol" 
        ]
    ][
        msg: "rebol - error"
    ]
    msg
]

format-message: func [user [object!] message [object!] color
    <local> userid username msgid replyto iconurl timestamp err msg content message-template monologue-template template
][
    reply-template: copy/deep [
        {<div class="user-container user-76852 monologue mine catchup-marker catchup-marker-1"><a href="/users/} userid
        {" class="signature user-} userid
        {"><div class="tiny-signature" style=""><div class="avatar avatar-16"><img width="16" height="16" src="}
        iconurl {alt="} username {" title="} username
        {"></div><div class="username owner">} username
        {</div></div><div class="avatar avatar-32 clear-both" style="display: none;"><img width="32" height="32" src="}
        iconurl
        {" alt="} username
        {" title="} username
        {"><div></div></div><div class="username owner" style="display: none;">}
        username
        {</div><div class="flair" title="} msgid {" style="display: none;">} msgid {</div></a><div class="messages"><div class="timestamp">} 9:14
        {</div><div class="message pid-} replyto
        {" id="message-} msgid
        {"><a class="action-link" title="click for message actions" href="/transcript/message/48724105#48724105">}
        {<span class="img menu"> </span></a><a class="reply-info" title="This is a reply to an earlier message" href="/transcript/message/} replyto "#" replyto {">}
        {</a><div class="content">} content
        {</div><span class="meta"><span class="flags vote-count-container"><span class="img"></span><span class="times"></span></span>&nbsp;<span class="stars vote-count-container"><span class="img"></span><span class="times"></span></span></span><span class="flash"><span class="stars vote-count-container"><span class="img"></span><span class="times"></span></span></span></div></div><div class="clear-both" style="height: 0px;">&nbsp;</div></div>}
    ]

    monologue-template: copy/deep [
        {<div class="user-container user-} (userid)
        { monologue"><a href="/users/} (userid)
        {" class="signature user-} (userid)
        {"><div class="tiny-signature" style=""><div class="avatar avatar-16"><img width="16" height="16" src="} 
        (iconurl)
        {" alt="} (username) 
        {" title="} (username)
        {"></div><div class="username">} (username)
        {</div></div><div class="avatar avatar-32 clear-both" style="display: none;"><img width="32" height="32" src="}
        (iconurl) 
        {" alt="} (username)
        {" title="} (username)
        {"><div></div></div><div class="username" style="display: none;">}
        (username)
        {</div><div class="flair" title="} msgid
        {" style="display: none;">} msgid
        {</div></a><div class="messages">}
        {<div class="timestamp">} (timestamp) </div>  
        {<div class="message" id="message-} msgid
        {"><a class="action-link" title="click for message actions" href="}
        {/transcript/message/48710297#48710297"><span class="img menu"> </span></a><div class="content">}
        (content)
        {</div><span class="meta"><span class="flags vote-count-container"><span class="img vote" title="flag this message as spam, inappropriate, or offensive">}
        {</span><span class="times"></span></span>&nbsp;<span class="stars vote-count-container">}
        {<span class="img vote" title="star this message as useful / interesting for the transcript"></span><span class="times"></span></span>&nbsp;<span class="newreply" title="link my next chat message as a reply to this">}
        {</span></span><span class="flash"><span class="stars vote-count-container"><span class="img vote" title="star this message as useful / interesting for the transcript"></span><span class="times"></span></span></span></div></div><div class="clear-both" style="height: 0px;">&nbsp;</div></div>}
    ]
        
    message-template: copy/deep [
         {<div class="user-container user-} (userid)
         { monologue mine catchup-marker catchup-marker-1">} 
         {<a href="/users/} (userid)
         {" class="signature user-} (userid)
         {"><div class="tiny-signature" style="display: none;"><div class="avatar avatar-16"><img width="16" height="16" src="}
         (iconurl)
         {" alt="} (username)
         {" title="} (username)
         {"></div><div class="username">} (username)
         {</div></div><div class="avatar avatar-32 clear-both" style=""><img width="32" height="32" src="}
         (iconurl)
         {" alt="} (username)
         {" title="} (username)
         {"><div></div></div><div class="username" style="display: block;">} (username)
         {</div><div class="flair" title="} msgid {" style="">} msgid {</div></a><div class="messages">}
         {<div class="timestamp">} (timestamp) </div>  
         {<div class="message" id="message-} (msgid)
         {"><a class="action-link" title="click for message actions" href="/transcript/message/48710755#48710755"><span class="img menu">}
         {</span></a><div class="content"><div class="full">} (content)
         {</div></div><span class="meta"><span class="flags vote-count-container"><span class="img"></span><span class="times"></span></span>&nbsp;}
         {<span class="stars vote-count-container"><span class="img"></span><span class="times"></span></span></span><span class="flash"><span class="stars vote-count-container">}
         {<span class="img"></span><span class="times"></span></span></span></div></div><div class="clear-both" style="height: 0px;">&nbsp;</div></div>}
    ]
    -- "Inside Format-message"
    probe user
    if err: trap [
        timestamp: simple-date message/date
        -- timestamp
        iconurl: user/iconurl
        -- iconurl
        userid: user/userid
        -- userid
        username: user/username
        -- username
        msgid: message/messageid
        -- msgid
        replyto: message/replyto
        -- replyto
        content: image-link youtuberfiy simple-md defang-html remove-apostrophes message/content
        -- content
      ; iconurl: default [""]
      -- "composing on message"
      template: copy case [
        integer? replyto [reply-template]
        color [message-template]
        default [monologue-template]
      ]
      msg: unspaced compose template
      -- "after composing message, now probe"
        print ["Type: " type-of msg]
        print ["Length: " length-of msg]
        ; probe msg
    ][
        -- "format error died"
        msg: mold err
        ; probe msg
    ]
    return msg
]

 simple-date: func [d [date!]
    <local> ds day month year n
][
    n: now
    day: d/day
    year: d/year
    parse form d/6 [thru "-" copy month to "-"]
    ds: copy if d/date = n/date ["Today"] else [spaced [month day if year <> n/year [year]]]
    append ds unspaced [space d/hour ":" next form 100 + d/minute]
    ds
]
 
 defang-html: func [txt][
    replace/all txt "<" "&lt;"
    replace/all txt ">" "&gt;"
    txt
 ]

remove-apostrophes: func [txt][
    take/last txt
    remove/part txt 1
]

image-link: func [txt
][
    if did parse txt ["http" opt "s" "://" [thru ".png" | thru ".jpg" | thru ".gif"] end][
        txt: unspaced [
            {<div class="onebox ob-image"><a rel="nofollow noopener noreferrer" href="}
            txt
            {"><img src="} 
            txt
            {" class="user-image" alt="user image"></a></div>}
        ]
    ]
    txt
]

; image-link https://imgs.xkcd.com/comics/troubleshooting_2x.png

 youtuberfiy: func [txt
    <local> link img title
 ][
     ; https://www.youtube.com/watch?v=YnmaBnzYqYg&t=439s
    if parse txt ["https://" opt "www." "youtube.com/watch?v=" copy link [to "&" | to end] to end][
        img: unspaced ["https://i2.ytimg.com/vi/" link "/hqdefault.jpg"]
        title: fetch-youtube-title link youtube-key ; func [ youtube-id youtube-key [text!]
        txt: unspaced [
            <div class="onebox ob-youtube">
            {<a rel="nofollow noopener noreferrer" style="text-decoration: none;" href="https://www.youtube.com/watch?v=}
            link {">}
            {<img src="} img {" }
            {width="240" height="180" class="ob-youtube-preview"><div class="ob-youtube-title">} title {</div><div class="ob-youtube-overlay">►</div></a></div>}
        ]
    ]
    txt
 ]
 
 simple-md: func [txt [text!]
    <local> mark1 mark2 mark3 tag1 tag2 tag3 rule secure
][
    pairs: ["__" <strong> "_" <i> "**" <strong> "*" <i> ]
    ; non-blank: complement charset newline
    ; chars: [some non-blank]
    
    for-each [mark tag] pairs [
        mark1: join " " mark
        mark2: join mark " "
        mark3: join mark ","
        tag1: join " " form tag
        ; -- tag1
        tag2: head insert copy tag '/
        ; insert tag2 '/
        ; -- tag2
        tag2: append form tag2 " "
        tag3: append form tag2 ","
        ; -- tag2
        rule: [thru mark1 [copy t to mark2 (
                if not did find t newline [
                    replace txt mark1 tag1
                    replace txt mark2 tag2
                ])  
                | copy t to mark3 (
                    if not did find t newline [
                        replace txt mark1 tag1
                        replace txt mark3 tag3
                    ] 
                )
            ]
        ]
        while [did parse txt rule][]
    ]
    ; now html links
    links-rule: [ (secure: false)
        thru "[" copy t to "](http" "](http" opt ["s" (secure: true)] "://" copy link to ")" 
        (
            replace txt 
                unspaced [ "[" t "]" ] 
                unspaced ["<a href=http" if secure ["s"] "://" link ">" t </a>]
            replace txt unspaced [ "(http" if secure ["s"] "://" link ")"] ""
        )
    ]
    while [did parse txt links-rule][]
    txt
]

; modified Brian Ottos' version
makeUUID: func [
    "Generates a Version 4 UUID that is compliant with RFC 4122"
    <local> data
][
    ; generate 16 random integers

    ; Note: REBOL doesn't support bytes directly 
    ; and so instead we pick numbers within the
    ; unsigned, 8-bit, byte range (0 - 255)

    ; Also random normally begins the range at 1, 
    ; not 0, and so -1/256 allows 0 to be picked
    data: collect [loop 16 [keep -1 + random 256]]

    ; set the first character in the 7th "byte" to always be 4
    data/7: data/7 and+ 15 or+ 64

    ; set the first character in the 9th "byte" to always be 8, 9, A or B
    data/9: data/9 and+ 63 or+ 128

    ; convert the integers to hexadecimal
    data: enbase/base to binary! data 16

    ; add the hyphens between each block 
    parse data [8 skip insert "-" 3 [5 skip insert "-"] to end]
    data
]


validate-registration: func [request [binary!]
    <local> retval msg err ck
][
    if parse to text! request [thru "gitlink=" copy git to end][
        dump git
        if err: trap [
            data: to text! lib/read to url! git
        ][
            return unspaced ["-ERR:" newline newline err]
        ]
        public-key: message: _
        parse data [
            to "-----BEGIN PGP PUBLIC KEY BLOCK-----" copy public-key thru "-----END PGP PUBLIC KEY BLOCK-----" 
            to "-----BEGIN PGP MESSAGE-----" copy message thru "-----END PGP MESSAGE-----" 
            to end
        ]
        if all [public-key message 100 < length-of public-key 100 < length-of message][
            ; dump public-key
            ; render message
            ; first see if we can decode the message, let's create a unique temp file
            lib/write filename: append to-file makeuuid %.gpg message
            decrypted: append copy filename %.asc
            ; gpg -d --batch --passphrase "secret-phrase-with-your-keys" filename > decrypted
            script: spaced ["gpg --pinentry-mode=loopback --passphrase" unspaced [{"} secret {"}] "-d -o" decrypted filename] 
probe script
            retval: call script
            -- retval
            if retval > 0 [
                return "-ERR: the details were not encrypted with the server's public key"
            ]
            append decrypted-txt: to text! lib/read decrypted newline
            
            ; dump decrypted-txt
            
            password: email: name: username: _
            -- "pre-parse"
            for-each [key value] ["password:" password "email:" email "full name:" name "username:" username][
            	parse decrypted-txt compose [thru (key) any space copy (value) to newline]
            ]
            delete filename
            delete decrypted
            valid: not did find reduce [password email name username] _
            print spaced ["we have a valid block of values:" valid]
            if valid [
                ; now we need to import the person's public key
                if err: entrap [
                    if email! = type-of load email [
                        if exists? to file! join "userdata/" username [
                            valid: false msg: copy "This userid already exists"
                        ] else [
                            ; now add to gpg database
                            lib/write decrypted public-key
                            retval: call spaced ["gpg --import" decrypted]
                            delete decrypted
                            if not zero? retval [
                                valid: false msg: copy "Failed to import your public key"
                            ] else [
                                ; now add to userdata, remove windows line endings
                                append replace/all decrypted-txt "^M" "" unspaced ["userid: " last-userno: me + 1 newline]
                                ck: lowercase form checksum/method to binary! email 'md5
                                remove/part ck 2
                                remove back tail ck
                                append users make object! compose [
                                    email: (email)
                                    userid: (last-userno)
                                    username: (username)
                                    lastread: copy initial-rooms
                                    sessionkey: _
                                    date: (now)
                                    iconurl: (join https://www.gravatar.com/avatar/ ck)
                                ]
                                attempt [
                                    lib/write to file! join "userdata/" username unspaced [trim/tail decrypted-txt newline "lastread: [" initial-rooms "]" newline]
                                ]
                            ]
                        ]
                    ] else [
                        valid: false msg: copy "Invalid Email value"
                    ]
                ] [
                    valid: false msg: join "An error occurred in processing your registration: " err
                ]
            ] else [
                valid: false msg: copy "registration text missing values"
            ]
            return if valid ["OK: Got a public key and message"] else [msg]
        ] else [
            return "wasn't able to parse the message"   
        ]
    ]
]

validate-login: func [request [binary!]
    <local> logintxt email user sessionfile script response
][
    logintxt: to text! request
    if did parse logintxt ["email=" copy email to [newline | end]][
        email: to email! url-decode email
        dump email
        if user: registered? email users [
            -- "registered pathway"
            ; generate sessionkey
            user/sessionkey: makeUUID
            user/date: now/precise
            probe user
            write sessionfile: to file! user/sessionkey user/sessionkey
            script: spaced ["gpg --trust-model always --recipient" email "--armour --encrypt" sessionfile]
            call script
            response: to text! read join sessionfile %.asc
            delete sessionfile
            delete join sessionfile %.asc
            return make object! compose [userid: (user/userid) sessionkey: (response)]
        ] else [return "Not registered"]
    ] else [
        return "malformed login request"
    ]
]

; render validate-speech request/binary room-no
; chat-object [username: [text!] userid: [integer!] sessionkey: [text!] content: [text!] replyto: [integer!]]

comment {
    {make object! [
        site: http://35.224.174.22
        sessionkey: "C102CB77-61D34-864A9-C67EA-0D940B862"
        username: "Graham"
        email: graham@somewhere.com
        room: 1
        userid: 1
        content: "hello sailor"
        replyto: _
    ]}
}

whois: func [ request [binary!]
    <local> user sessionkey u msg
][
    request: to text! request
    user: _
    if did parse request [thru "sessionkey:" any space thru {"} copy sessionkey to {"} to end][
        for-each u users [
            if u/sessionkey = sessionkey [
                user: u
                break
            ]
        ]
        ; got a registered user request
        if something? user [
            msg: whois-registered sessionkey   
        ] else [
            msg: "you need to be logged in to do this"
        ]
    ] else [
        msg: "Invalid format - you need to provide a sessionkey"
    ]
    msg
]

; request is molded chat-obj
validate-update-icon: func [ request [binary!] 
    <local> user msg url err err1 userfile sessionkey email
][
    -- "update icon"
    msg: copy "+OK"
    if err: entrap [
        request: to text! request
        probe request
        
        if did parse request [
            thru "sessionkey:" any space thru {"} copy sessionkey to {"}
            thru "email:" any space copy email to newline
            thru "iconurl:" any space copy url [to newline | to end]
            to end
        ][
            email: to email! email
            if user: registered? email users [
                -- "got the user object"
                if all [
                    sessionkey = user/sessionkey
                    email = user/email
                ][
                    ; confirmed user - let's update the icon
                    
                    ; now to check if the image exists at given url
                    if err1: entrap [
                        url: to url! url
                        -- url
                        if exists? url [
                            -- "image exists"
                            user/iconurl: url
                            ; got an image, now update the user record - this action should not fail
                            
                            record: trim/tail to text! lib/read userfile: to file! unspaced [what-dir "userdata/" user/username] 
                            -- "read the user record"
                            if did parse record [thru "iconurl:" any space "http" opt "s" "://" to end][
                                record: copy/part record find record "iconurl"
                                trim/tail record
                                append record unspaced [newline "iconurl=" url newline]
                                attempt [
                                    w* userfile record
                                    -- "successfully updated record"
                                    msg: copy "+OK updated icon record"
                                ]
                            ] else [
                                ; no iconurl
                                attempt [
                                    w*/append userfile unspaced ["iconurl: " url newline]
                                    msg: copy "+OK added icon to record"
                                ]
                            ]
                        ] else [ msg: "-ERR: image not found at that location"]
                    ][
                        msg: unspaced [ "-ERR: on accessing user record" newline newline err1]
                    ]
                ] else [
                    ; user not logged in or wrong email                
                    msg: copy "-ERR: you are not logged in under this email address"
                ]
            ] else [
                msg: join "-ERR: no account under this email of " email
            ]
        ] else [
            msg: copy "-ERR: wrong format to update icon"
        ]
    ][
        msg: unspaced ["-ERR: error decoding payload^/^/" err]
    ]
    return msg
]

validate-speech: func [request [binary!] room-no [integer!]
    <local> sessionkey username email userid content replyto msg user msgstore chatroom
][
    -- "message arrived"
    
    attempt [
        -- home
    ]
    request: to text! request
    msg: "+OK"
    -- "checking for room existence"
    probe chatroom: to file! unspaced [what-dir "rooms/" room-no "/"]
    if err: entrap [
        -- "entering trap"
        if not did exists? chatroom [
            -- "room doesn't exist"
            ; cd home
            return spaced ["-ERR: room" room-no "does not exist"]
        ]
    ][
        -- "error trapped"
        probe err
    ]
    -- "after room check"
    ; cd home << dies on this!!
    ; it should be a molded object.  We're not going to trust it so will just parse it
    -- "parsing request"
    if did parse request [
        thru "sessionkey:" any space thru {"} copy sessionkey to {"} 
        thru "username:" any space copy username to newline
        thru "email:" any space copy email to newline
        thru "userid:" any space copy userid numbers any space newline (userid: to integer! userid)
        thru "content:" any space copy content to newline
        thru "replyto:" any space copy replyto to newline
        to end
    ][
        -- "request parsed"
        dump [sessionkey username email userid content replyto]
        ; make sure replyto is an integer!
        parse replyto [copy replyto numbers to end (replyto: to integer! replyto)]
        replyto: default [_]
        ; now make sure that the userid/email corresponds to this sessionkey 
        if err: entrap [
            email: to email! email
            if user: registered? email users [
                -- "got the user object"
                if all [
                    sessionkey = user/sessionkey
                    userid = user/userid
                    email = user/email
                ][
                    -- "go to save message"
                    message: make object! compose [
                        userid: (userid)
                        date: (now/precise)
                        room: (room-no)
                        messageid: (maxmessageno: me + 1)
                        content: (content)
                        replyto: (replyto)
                    ]
                    -- "update timestamp on user"
                    user/date: now/precise
                    
                    -- "saving maxmessageno"
                    save %maxmessageno.reb maxmessageno
                    msgstore: to file! unspaced [chatroom next form 100'000'000 + maxmessageno %.reb]
                    dump msgstore
                    save msgstore message ;;<< errors here after saving the message, but at least it saves!
                    ; cd home << does home exist here?
                ] else [
                    -- "problem not matching keys"
                    -- sessionkey
                    -- userid
                    -- email
                    probe user
                    msg: copy "-ERR: there was a failure to match the sessionkey, email or userid with the registered user"
                ]
            ] else [
                msg: spaced ["-ERR: This email account of" email "is no longer associated with an account."]
            ]
        ][
            ; for some unknown reason a File Access error is being thrown when message is written
            if not find mold err {title: "File Access"} [
                msg: join "-ERR: processing error occurred: ^/^/" err
            ]
        ]
    ] else [
        msg: copy "-ERR: not a valid message object"
    ]
    return msg
]

process-gets: func [request
    <local> target err cell num
][
    -- "entering process-gets"
    allowed-gets: [
        %"" %/ %index.html %logs/log.txt %rebolchat.asc %robots.txt
    ]

    if did target: find/tail request/action "GET /" [
        case [
            any [empty? target target = "/"][return read %index.html]
            
            did parse target ["logs" opt "/" end][return unspaced [<pre> to text! read %logs/log.txt </pre>]]

            target = "favicon.ico" [return lib/read %favicon.ico]
            
            did parse target ["rooms/" [copy num numbers | "sandbox" (num: 1) | "rebol" (num: 2)] opt "/" end][
                ; directory read
                num: to integer! num
                target: to file! join "rooms/" num
                if exists? target [
                    -- "directory exists"
                    use [last-userid userid messages file message output chatroom user name][
                        chatroom: dirize to file! unspaced [what-dir target]
                        -- chatroom
                        -- "read directory"
                        -- target
                        messages: lib/read chatroom
                        -- "sort directory"
                        messages: sort messages
                        probe messages
                        roomname: switch num [
                            1 ["Sandbox"]
                            2 ["Rebol"]
                            default [num]
                        ]
                        output: unspaced [ {<html><head>^/<meta charset="UTF-8">^/<link rel="stylesheet" href="https://cdn-chat.sstatic.net/chat/css/chat.stackoverflow.com.css?v=37edb15d87cc"></head><body><h2> Message List } roomname {</h2>}]
;-- output
                        last-userid: 0
                        for-each file messages [
                            if not dir? file [
                                -- file
                                message: do load to text! lib/read to file! unspaced [chatroom file]
                                probe type-of message
                                probe message
                                name: copy "Unknown"
                                userid: message/userid
                                for-each u users [
                                    if userid = u/userid [
                                        name: copy u/username
                                        user: u
                                        break
                                    ]
                                ]

                                ; should check to see if user is still registered or removed after they registered
                                probe user
                                
                                if err: trap [
                                    -- "before format-message"
                                    color: (last-userid = userid)
                                    cell: format-message user message color
                                    last-userid: userid
                                    ; -- "Dumping cell"
                                    ; probe cell
                                    ; -- "after format-message"
                                ][
                                    -- "in error after format-message"
                                    probe err
                                ]
                                append output cell
                            ]
                        ]
                        -- "TO be sent to user"
                        ; -- output
                        ; probe output
                        return append output {</body></html>}
                    ]
                ] else [
                    return [status: 404 content: "chat room not found"]
                ]
            ]

            did parse target ["rooms/" numbers "/" numbers ".reb" to end][
                -- "Parsed a room message request"
                target: to file! target
                dump target
                if exists? target [
                    -- "Found the file"
                    return to text! read target
                ] else [ 
                    return [status: 404 content: "chat message not found"]
                ]
            ]

            all [
                target: to file! target
                did find allowed-gets target
                exists? target 
            ][
                if %.txt = suffix? target [open-tag: copy <pre> close-tag: copy </pre>] else [open-tag: close-tag: _]
                return unspaced [ open-tag to text! read target close-tag ]
            ]
            
            default [
                return [status: 404 content:  join "This website is devoid even of " target]
            ]
        ]
    ] else [
        ; eg GET http://110.249.212.46/testget
        return [status: 500 content:  "Malformed request from client"]
    ]
]
